# Creating a New Repository

Before creating the repository, decide the appropriate access level over the long term.

* Public: Most of our repositories should be publicly available.
* Customer-Only: My contain data that we do not want available on a web search, but which we do want to share with customers. This includes binary artifacts, policies about internal systems, and product repositories that we exclude from our open source policy.
* Internal-only: Repositories that contain customer or employee data that cannot be shared.

In general, our engineering team works at GitLab.com. But to ensure broad visibility for our work, we create a read-only mirror in GitHub.


# Repository Structure

Our public repos should be friendly for developers to use and contribute to. Look to open source projects at Hyperledger for examples of best practices.

* /README.md
  * Should explain the purpose of the repository.
  * Should explain that people who want help from Evernym should contact us about our support offering.
  * Contains instructions for making a contribution.
  * A link to our [Community Code of Conduct](http://github.com/evernym/evernym-docs/blob/main/Code-of-Conduct.md).
* /LICENSE.txt
  * The default license is the [Evernym BSL](EvBSL.txt).
  * Under Product Management direction, it might be appropriate to use the Apache 2.0 license for a public repository.
  * Internal-only repos, and customer-only repos that are excluded from our open source policy should be "All Rights Reserved".
  * In the unlikely event that we change the Evernym BSL, we will have to upload the new version to every repository.
* /docs
  * Containing project documentation.
* /docs/design
  * Containing design documents.
* /CHANGELOG.md
  * For each release, contains the version number and date of the release.
  * Then has a summary of major changes with each release.
  * Newest releases are at the top.
* /SECURITY.md
  * Contains instructions for reporting a security vulnerability to support@evernym.com

The default branch should be "main".


# Pipelines

* Pipelines should be public, so that contributors can see the tests that are performed on their Merge Requests and fix issues before asking for review.
* Artifacts should be public.
* Special pipeline tests:
  * Confirm that external contributors agreed to the [Evernym Contributor License Agreement](EvCLA.txt)
  * Confirm that the LICENSE_CHANGE_DATE.txt is within 30 days of the third anniversary of the most recent commit to Main in the repo (signifying that the license changes to Apache 3 years after the commit).
    * A Git pre-commit hook installed in the team's development environment can automatically update the LICENSE_CHANGE_DATE.


# GitHub Repositories

* Need a clear name.
* A description that describes the repository and states "This is a read-only mirror. Development occurs at [link to GitLab.com]"
* A web link to the most relevant page for the product
  * GitLab repo where development happens.
  * Evernym page for the product.
  * http://evernym.com
* Topic tags such as: self-sovereign-identity, digital-identity
* Disable:
  * Wikis
  * Issues
  * Sponsorships
  * Projects
